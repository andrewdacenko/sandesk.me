var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// var redis = require('redis');
// var db = redis.createClient();

// app.use(function(req, res, next) {
//     var ua = req.headers['user-agent'];
//     db.zadd('online', Date.now(), ua, next);
// });

// app.use(function(req, res, next) {
//     var min = 60 * 1000;
//     var ago = Date.now() - min;
//     db.zrevrangebyscore('online', '+inf', ago, function(err, users) {
//         if (err) return next(err);
//         req.online = users;
//         next();
//     });
// });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
// app.use('/', function(req, res, next) {
//     res.sendfile('views/index.html', {
//         root: __dirname
//     })
// });
// app.use('/users', users);

// app.get('/users', function(req, res) {
//     res.send(req.online.length + ' users online');
// });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;