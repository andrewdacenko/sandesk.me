var express = require('express');
var router = express.Router();
var parseurl = require('parseurl');

var neo4j = require('node-neo4j');
db = new neo4j('http://localhost:7474');

/* GET home page. */
router.get('/', function(req, res) {
	// res.render('index', { title: 'Express' });
	res.sendFile('views/index.html', {
		root: __dirname + '/../'
	})
});

router.param('id', function(req, res, next, id) {
	if (!id) {
		return next(new Error('failed to load hash'));
	}

	if (id.indexOf('//') === 0) {
		id = "%2F%2F" + id.substr(2, id.length);
	}

	// id.replace(':/', encodeURIComponent(':/'));

	var hash = escape(id);

	if (hash === '.') {
		hash = 'root';
	}
	if (hash === '..') {
		hash = 'parent';
	};

	var find = [
		"MATCH (n:Hash)",
		"WHERE n.hash = '" + hash + "'",
		"RETURN n"
	].join('\n');

	// SEARCHING
	db.cypherQuery(find, function(err, result) {
		if (err) return next(err);

		if (result.data.length === 0) {
			var create = [
				'CREATE (hash:Hash {hash:"' + hash + '"})',
				'RETURN hash'
			].join('\n');

			// CREATING
			db.cypherQuery(create, function(err, result) {
				if (err) return next(err);

				req.id = id;

				next();
			});
		} else {
			req.id = id;
			next();
		}
	});

});

router.get('/:id*', function(req, res, next) {

	res.sendFile('views/index.html', {
		root: __dirname + '/../'
	})
});

router.post('/', function(req, res, next) {
	var top = false;

	if (req.body.top) {
		if (parseInt(req.body.top) === 1) {
			top = true;
			// Most commentable
			var find = [
				"MATCH (e)-[r:COMMENTS]->(n)",
				"WITH n,count(r) AS c ",
				"ORDER BY c DESC",
				"RETURN n",
				"LIMIT 20",
			].join('\r');

			db.cypherQuery(find, function(err, result) {
				if (err) return next(err);

				res.send(JSON.stringify(result.data));
			});
		} else if (parseInt(req.body.top) === 2) {
			top = true;
			// Most usable
			var find = [
				"MATCH (e)-[r:COMMENTS]->(n)",
				"WITH e,count(r) AS c ",
				"ORDER BY c DESC",
				"RETURN e LIMIT 20",
			].join('\r');

			db.cypherQuery(find, function(err, result) {
				if (err) return next(err);

				console.log(result);

				return res.send(JSON.stringify(result.data));
			});
		} else {
			top = false;
		}
	}

	console.log('top', top, req.body.top);

	if (!top) {
		console.log('In hash top', top, req.body.top);

		if (!req.body.hash) {
			return next(new Error('Provide hash'));
		};

		if (!req.body.comment) {
			return next(new Error('Provide comment'));
		};


		// if (req.body.hash.indexOf('//') === 0) {
		// 	req.body.hash = "%2F%2F" + req.body.hash.substr(2, req.body.hash.length);
		// }

		// req.body.hash.replace(':/', encodeURIComponent(':/'));

		var hash = escape(req.body.hash);


		// if (req.body.comment.indexOf('//') === 0) {
		// 	req.body.comment = "%2F%2F" + req.body.comment.substr(2, req.body.comment.length);
		// }

		// req.body.comment.replace(':/', encodeURIComponent(':/'));

		var comment = escape(req.body.comment);

		// var hash = escape(encodeURIComponent(req.body.hash));
		// var hash = escape(encodeURIComponent(req.body.hash));
		// var comment = escape(encodeURIComponent(req.body.comment));
		// var comment = escape(encodeURIComponent(req.body.comment));

		var find = [
			"MATCH (n:Hash)",
			"WHERE n.hash = '" + hash + "'",
			"RETURN n"
		].join('\n');

		// SEARCHING
		db.cypherQuery(find, function(err, result) {
			if (err) {
				return next(err);
			}

			if (result.data.length === 0) {
				return next(new Error('Too bad hash'));
			} else {
				find = [
					"MATCH (n:Hash)",
					"WHERE n.hash = '" + comment + "'",
					"RETURN n"
				].join('\n');

				db.cypherQuery(find, function(err, result) {
					if (err) {
						return next(err);
					}

					if (result.data.length === 0) {
						var create = [
							'CREATE (hash:Hash {hash:"' + comment + '"})',
							'RETURN hash'
						].join('\n');

						// CREATING
						db.cypherQuery(create, function(err, result) {
							if (err) return next(err);

							var rel = [
								"MATCH (c:Hash),(h:Hash)",
								"WHERE c.hash = '" + comment + "' AND h.hash = '" + hash + "'",
								"CREATE (c)-[r:COMMENTS]->(h)",
								"RETURN r"
							].join('\n');

							db.cypherQuery(rel, function(err, rel) {
								if (err) return next(err);

								res.send(JSON.stringify(result.data[0]));
							});
						});
					} else {
						var rel = [
							"MATCH (c:Hash),(h:Hash)",
							"WHERE c.hash = '" + comment + "' AND h.hash = '" + hash + "'",
							"CREATE (c)-[r:COMMENTS]->(h)",
							"RETURN r"
						].join('\n');

						db.cypherQuery(rel, function(err, rel) {
							if (err) return next(err);

							res.send(JSON.stringify(result.data[0]));
						});
					}
				});

			}
		});
	};
});

router.post('/:id*', function(req, res, next) {
	var hash = req.id;

	var find = [
		"MATCH (n)-[r:COMMENTS]->(c { hash: '" + hash + "' })",
		"RETURN n",
		"ORDER BY n._id DESC"
		// "LIMIT 100"
	].join('\n');

	db.cypherQuery(find, function(err, result) {
		if (err) return next(err);

		res.send(JSON.stringify(result.data));
	});
});

module.exports = router;
