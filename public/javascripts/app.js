(function() {
    var app = angular.module('sandesk', ['ngRoute']);

    app.config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.
            when('/', {
                templateUrl: '/javascripts/main.html',
                // controller: 'MainCtrl'
            }).
            when('/:hashId', {
                templateUrl: '/javascripts/hash.html',
                // controller: 'HashCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });

            $locationProvider.html5Mode(true);
        }
    ]);

    app.controller('MainCtrl', function($scope) {
        this.data = 'mainC';
    });

    app.controller('HashCtrl', ['$scope', '$location', '$http',
        function($scope, $location, $http) {
            var self = this;
            // console.log('Call hash ctrl', new Date())
            self.comments = [];
            self.hash = $location.path().substr(1, $location.path().length - 1);
            $http.post('/' + self.hash).success(function(response) {
                self.comments = response;
            }).error(function(response) {
                console.log("Server connect error: " + response);
            });
        }
    ]);

    app.filter('unescape', function() {
        return function(text) {
            return unescape(text);
        }
    });

    app.controller('SearchCtrl', ['$scope', '$location', '$http',
        function($scope, $location, $http) {
            this.input = '';
            this.searchHash = function() {
                $location.path('/' + this.input);
            };
        }
    ]);

    app.controller('CommentCtrl', ['$scope', '$location', '$http',
        function($scope, $location, $http) {
            var self = this;
            self.input = '';
            self.comment = function(pageHash, hashCtrl) {
                var data = {
                    comment: self.input,
                    hash: pageHash
                };
                $http.post('/', data).success(function(response) {
                    hashCtrl.comments.push(response);
                }).error(function(response) {
                    console.log("Server connect error: " + response);
                });
                self.input = '';
            };
        }
    ]);

    app.controller('LeaderCtrl', ['$scope', '$location', '$http',
        function($scope, $location, $http) {
            var self = this;
            self.commentable = [];
            self.usable = [];
            self.hash = $location.path().substr(1, $location.path().length - 1);
            $http.post('/', {
                top: 1
            }).success(function(response) {
                console.log(response);
                self.commentable = response;
            }).error(function(response) {
                console.log("Server connect error: " + response);
            });
            $http.post('/', {
                top: 2
            }).success(function(response) {
                console.log(response);
                self.usable = response;
            }).error(function(response) {
                console.log("Server connect error: " + response);
            });
        }
    ]);

})();