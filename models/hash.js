// hash.js
// Hash model logic.

var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase(
    process.env['NEO4J_URL'] ||
    process.env['GRAPHENEDB_URL'] ||
    'http://localhost:7474'
);

// private constructor:

var Hash = module.exports = function Hash(_node) {
    // all we'll really store is the node; the rest of our properties will be
    // derivable or just pass-through properties (see below).
    this._node = _node;
}

// public instance properties:

Object.defineProperty(Hash.prototype, 'id', {
    get: function() {
        return this._node.id;
    }
});

Object.defineProperty(Hash.prototype, 'hash', {
    get: function() {
        return this._node.data['hash'];
    },
    set: function(hash) {
        this._node.data['hash'] = hash;
    }
});

// public instance methods:

Hash.prototype.save = function(callback) {
    this._node.save(function(err) {
        callback(err);
    });
};

Hash.prototype.del = function(callback) {
    // use a Cypher query to delete both this hash and his/her following
    // relationships in one transaction and one network request:
    // (note that this'll still fail if there are any relationships attached
    // of any other types, which is good because we don't expect any.)
    var query = [
        'MATCH (hash:Hash)',
        'WHERE ID(hash) = {hashId}',
        'DELETE hash',
        'WITH hash',
        'MATCH (hash) -[rel:follows]- (other)',
        'DELETE rel',
    ].join('\n')

    var params = {
        hashId: this.id
    };

    db.query(query, params, function(err) {
        callback(err);
    });
};

Hash.prototype.follow = function(other, callback) {
    this._node.createRelationshipTo(other._node, 'follows', {}, function(err, rel) {
        callback(err);
    });
};

Hash.prototype.unfollow = function(other, callback) {
    var query = [
        'MATCH (hash:Hash) -[rel:follows]-> (other:Hash)',
        'WHERE ID(hash) = {hashId} AND ID(other) = {otherId}',
        'DELETE rel',
    ].join('\n')

    var params = {
        hashId: this.id,
        otherId: other.id,
    };

    db.query(query, params, function(err) {
        callback(err);
    });
};

Hash.prototype.getFollowers = function(callback) {
    this._node.getRelationships('incoming', function(err, results) {
        console.log('followers err', err);
        if (err) return callback(err);

        callback(null, results);
    });
};

Hash.prototype.createIndex = function(hash, callback) {
    this._node.index('hashes', 'hash', hash, function(err) {
        callback(err);
    });
};

// calls callback w/ (err, following, others) where following is an array of
// hashs this hash follows, and others is all other hashs minus him/herself.
Hash.prototype.getFollowingAndOthers = function(callback) {
    // query all hashs and whether we follow each one or not:
    var query = [
        'MATCH (hash:Hash), (other:Hash)',
        'OPTIONAL MATCH (hash) -[rel:follows]-> (other)',
        'WHERE ID(hash) = {hashId}',
        'RETURN other, COUNT(rel)', // COUNT(rel) is a hack for 1 or 0
    ].join('\n')

    var params = {
        hashId: this.id,
    };

    var hash = this;
    db.query(query, params, function(err, results) {
        if (err) return callback(err);

        var following = [];
        var others = [];

        for (var i = 0; i < results.length; i++) {
            var other = new Hash(results[i]['other']);
            var follows = results[i]['COUNT(rel)'];

            if (hash.id === other.id) {
                continue;
            } else if (follows) {
                following.push(other);
            } else {
                others.push(other);
            }
        }

        callback(null, following, others);
    });
};

// static methods:

Hash.get = function(id, callback) {
    db.getNodeById(id, function(err, node) {
        if (err) return callback(err);
        callback(null, new Hash(node));
    });
};

Hash.find = function(hash, callback) {
    db.getIndexedNode('hashes', 'hash', hash, function(err, node) {
        console.log('indexed err:', err);
        if (err) return callback(err);

        callback(null, new Hash(node));
    });
};

Hash.getAll = function(callback) {
    var query = [
        'MATCH (hash:Hash)',
        'RETURN hash',
    ].join('\n');

    db.query(query, null, function(err, results) {
        if (err) return callback(err);
        var hashs = results.map(function(result) {
            return new Hash(result['hash']);
        });
        callback(null, hashs);
    });
};

// creates the hash and persists (saves) it to the db, incl. indexing it:
Hash.create = function(data, callback) {
    // construct a new instance of our class with the data, so it can
    // validate and extend it, etc., if we choose to do that in the future:
    var node = db.createNode(data);
    var hash = new Hash(node);

    // but we do the actual persisting with a Cypher query, so we can also
    // apply a label at the same time. (the save() method doesn't support
    // that, since it uses Neo4j's REST API, which doesn't support that.)
    var query = [
        'CREATE (hash:Hash {data})',
        'RETURN hash',
    ].join('\n');

    var params = {
        data: data
    };

    db.query(query, params, function(err, results) {
        if (err) return callback(err);
        console.log('res', results);
        var hash = new Hash(results[0]['hash']);

        hash.createIndex(hash.hash, function(err) {
            callback(err);
        });

        callback(null, hash);
    });
};