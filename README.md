# SANDESK.ME #

This is the project made in 12 hours for fun with new technologies and backend developer at frontend & frontend developer at backend.

### How do I get set up? ###

* Install node.js
* Clone
* Run `npm install`
* Download `neo4j`
* Start neo4j
* Start `node bin/www`
* Navigate `http://localhost:45301`

### Contacts ###

* https://bitbucket.org/andrewdacenko
* https://bitbucket.org/Detonavomek